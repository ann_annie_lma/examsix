package com.example.examsix

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.examsix.adapter.PropertyAdapter
import com.example.examsix.databinding.FragmentPropertyBinding
import com.example.examsix.viewmodel.PropertyViewModel


class PropertyFragment : BaseFragment<FragmentPropertyBinding,PropertyViewModel>(FragmentPropertyBinding::inflate) {


    override fun getViewModelClass() = PropertyViewModel::class.java
    override var useSharedViewModel = true
    lateinit var propertyAdapter: PropertyAdapter

    override fun setUprecyclerView() {
        binding.rvProperty.apply {
            propertyAdapter = PropertyAdapter()
            adapter = propertyAdapter
            layoutManager = LinearLayoutManager(context)

        }
    }

    override fun callFromViewModel() {
        viewModel.response.observe(viewLifecycleOwner,{response ->
            if(response.isSuccessful)
            {
                Toast.makeText(context,"Success",Toast.LENGTH_SHORT).show()
                propertyAdapter.properties = response.body()?.content!!
                Toast.makeText(context,"${response.body()?.content!!.size}",Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun observer() {
        viewModel.getProperties()
    }

}