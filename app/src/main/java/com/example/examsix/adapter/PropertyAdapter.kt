package com.example.examsix.adapter

import android.icu.number.NumberFormatter.with
import android.icu.number.NumberRangeFormatter.with
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.examsix.PropertyFragment
import com.example.examsix.R
import com.example.examsix.databinding.PropertyItemBinding
import com.example.examsix.model.Prop
import com.squareup.picasso.Picasso

class PropertyAdapter : RecyclerView.Adapter<PropertyAdapter.PropertyViewHolder>() {



    inner class PropertyViewHolder(val binding: PropertyItemBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun bind()
        {
            binding.apply {
                val property = properties[adapterPosition]
                tvDate.text = property.publishDate
                tvTitle.text = property.titleKA
                tvDescription.text = property.descriptionKA

                
            }
        }
    }


    private val diffCallBack = object: DiffUtil.ItemCallback<Prop.Content>()
    {
        override fun areItemsTheSame(oldItem: Prop.Content, newItem: Prop.Content): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Prop.Content, newItem: Prop.Content): Boolean {
            return oldItem == newItem
        }

    }

    private val differ = AsyncListDiffer(this,diffCallBack)
    var properties : List<Prop.Content>
        get() = differ.currentList
        set(value) {differ.submitList(value)}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PropertyViewHolder {
        return PropertyViewHolder(
            PropertyItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ))
    }

    override fun onBindViewHolder(holder: PropertyViewHolder, position: Int) {
       holder.bind()
    }

    override fun getItemCount() = properties.size
}