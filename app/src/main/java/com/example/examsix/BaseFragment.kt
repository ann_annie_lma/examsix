package com.example.examsix

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding

typealias inflation<T> = (LayoutInflater, ViewGroup, Boolean) -> T

abstract class BaseFragment<VB : ViewBinding,VM: ViewModel>(private val inf:inflation<VB>) : Fragment() {


    open var useSharedViewModel = false
    protected lateinit var viewModel: VM
    protected abstract fun getViewModelClass() : Class<VM>

    private var _binding:VB? = null
    val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inf.invoke(inflater, container!!, false)
        callFromViewModel()
        observer()
        setUprecyclerView()

        return _binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }



    abstract fun setUprecyclerView()

    abstract fun callFromViewModel()

    abstract fun observer()



    private fun init()
    {
        viewModel = if(useSharedViewModel)
        {
            ViewModelProvider(requireActivity()).get(getViewModelClass())
        } else
        {
            ViewModelProvider(this).get(getViewModelClass())
        }
    }
}