package com.example.examsix.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.examsix.api.RetrofitInstance
import com.example.examsix.model.Prop
import kotlinx.coroutines.launch
import retrofit2.Response

class PropertyViewModel : ViewModel() {

    var response: MutableLiveData<Response<Prop>> = MutableLiveData()

    private suspend fun getPropertiesFrom() : Response<Prop>
    {
        return RetrofitInstance.api.getResult()
    }

    fun getProperties()
    {
        viewModelScope.launch{
            response.value = getPropertiesFrom()

        }
    }
}