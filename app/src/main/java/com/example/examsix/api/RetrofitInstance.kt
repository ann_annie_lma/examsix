package com.example.examsix.api

import com.example.examsix.constants.Constants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {

    val api:PropertyApi by lazy {
        Retrofit.Builder().
        baseUrl(Constants.baseUrl).
        addConverterFactory(GsonConverterFactory.create()).
        build().create(PropertyApi::class.java)

    }
}