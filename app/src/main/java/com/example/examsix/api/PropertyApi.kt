package com.example.examsix.api

import com.example.examsix.model.Prop
import retrofit2.Response
import retrofit2.http.GET

interface PropertyApi {

    @GET("/v3/f4864c66-ee04-4e7f-88a2-2fbd912ca5ab")
    suspend fun getResult(): Response<Prop>
}